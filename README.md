

# MovieFinderAPI

Ce projet est le dépot de notre API Restful dans lequel nous stockons et diffusons des id de films que nous pouvons annoter d'un bandeau "Coup de coeur" au sein de l'appli MovieFinder développé en React Native : https://codefirst.iut.uca.fr/git/lucas.delanier/MovieFinder

<p align="center">
  <img src="https://codefirst.iut.uca.fr/git/lucas.delanier/moviefinder_api/raw/branch/master/Documentation/doc_images/exemple.png" width="400" />
</p>
