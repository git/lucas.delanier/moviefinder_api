﻿
using Microsoft.AspNetCore.Mvc;

namespace MovieFinder_API.controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class SuggestedController : Controller
    {

        public int[] suggestedMovies = new int[]
        {
            677179,315162,76600
        };
        // GET: SuggestedController
        [Produces("application/json")]
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            return Ok(suggestedMovies);
        }

       
    }
}
